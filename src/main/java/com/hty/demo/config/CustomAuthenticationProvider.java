package com.hty.demo.config;

import com.hty.demo.bean.po.CustomUserInfo;
import com.hty.demo.service.UserService;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

import java.util.Objects;

public class CustomAuthenticationProvider implements AuthenticationProvider {

    private UserService userService;

    public CustomAuthenticationProvider(UserService userService) {
        this.userService = userService;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        if (null == authentication.getPrincipal() || !(authentication.getPrincipal() instanceof String)) {
            throw new InternalAuthenticationServiceException("authentication#getPrincipal() return unexpected value type '"
                    + authentication.getPrincipal().getClass().getName() +"' which should be type of 'String'");
        }
        String userName = (String) authentication.getPrincipal();
        CustomUserInfo customUserInfo = userService.loadUserByUsername(userName);
        if (customUserInfo == null || !Objects.equals(customUserInfo.getPassword(), authentication.getCredentials())) {
            throw new AuthenticationCredentialsNotFoundException("invalid username/password");
        }
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                new UsernamePasswordAuthenticationToken(customUserInfo.getUserInfo(), null, customUserInfo.getAuthorities());
        return usernamePasswordAuthenticationToken;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return true;
    }
}
