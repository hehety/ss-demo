package com.hty.demo.config;

import com.hty.demo.bean.po.CustomUserInfo;
import com.hty.demo.bean.po.UserInfo;
import com.hty.demo.util.CookieUtil;
import com.hty.demo.util.SimpleCacheUtil;
import com.hty.demo.util.TokenGeneratorUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CustomLoginFilter extends AbstractAuthenticationProcessingFilter {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private AuthenticationManager authenticationManager;


    public CustomLoginFilter(String defaultFilterProcessesUrl,
                             AuthenticationManager authenticationManager) {
        super(defaultFilterProcessesUrl);
        this.authenticationManager = authenticationManager;
    }



    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException, IOException, ServletException {
        log.info("attemptAuthentication-尝试登录");

        String username = request.getParameter("username");
        String password = request.getParameter("password");
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                new UsernamePasswordAuthenticationToken(username, password);
        Authentication authentication = authenticationManager.authenticate(usernamePasswordAuthenticationToken);
        return authentication;
    }


    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
        log.info("authenticated success");
        // TODO 将用户会话信息保存到redis...
        String xtoken = CookieUtil.getCookieValue(request, "X-TOKEN");
        if (null == xtoken || "".equals(xtoken)) {
            xtoken = TokenGeneratorUtil.generateAppToken();
            Cookie cookie = new Cookie("X-TOKEN", xtoken);
            cookie.setDomain("localhost");
            cookie.setMaxAge(300);
            cookie.setPath("/");
            response.addCookie(cookie);
        }
        SimpleCacheUtil.cacheAuthentication(xtoken, authResult);
        response.getWriter().write("login success");
        response.getWriter().close();
    }

    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, AuthenticationException failed) throws IOException, ServletException {
        log.info("authenticated failed");
        response.getWriter().write("login failed");
        response.getWriter().close();
    }
}
