package com.hty.demo.config;

import com.hty.demo.service.UserService;
import com.hty.demo.util.CookieUtil;
import com.hty.demo.util.SimpleCacheUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.context.HttpRequestResponseHolder;
import org.springframework.security.web.context.SecurityContextPersistenceFilter;
import org.springframework.security.web.context.SecurityContextRepository;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;

import static org.springframework.security.web.context.HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY;

@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private final Logger log = LoggerFactory.getLogger(this.getClass());
    public static final String CONTEXT_SAVED = "CONTEXT_SAVED_BEFORE";

    @Autowired
    private UserService userService;


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                // 允许访问的资源
                .antMatchers("/login").permitAll()
                .antMatchers("/logout").permitAll()
                .antMatchers("/demo*").permitAll()
                .antMatchers("/login.html").permitAll()
                .antMatchers("/register").permitAll()
                .antMatchers("/about").permitAll()
                .antMatchers("/favicon.ico").permitAll()
                .antMatchers("/*").authenticated()
                .and().formLogin().disable()
                .csrf().disable()
                // 处理登录的filter
                .addFilterBefore(customAuthenticationProcessingFilter(authenticationManager()), SecurityContextPersistenceFilter.class)
                .logout().logoutUrl("/logout").deleteCookies("X-TOKEN").logoutSuccessUrl("/login.html")
                .and()
                // 权限校验异常处理
                .exceptionHandling()
                // 登录但没有资源的访问权限
                .accessDeniedHandler(new AccessDeniedHandler() {
                    @Override
                    public void handle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AccessDeniedException e) throws IOException, ServletException {
                        log.error(e.toString());
                        httpServletResponse.getWriter().write("you are login but you don't have permission to access the resource.");
                    }
                })
                // 没有登录
                .authenticationEntryPoint(new AuthenticationEntryPoint() {
                    @Override
                    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
                        log.error(authException.toString());
                        response.getWriter().write("access denied, please login!");
                    }
                })
                .and()
                // 禁止spring创建session
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.NEVER)
                .and()
                // securityContext 恢复会话
                .securityContext().securityContextRepository(new SecurityContextRepository() {
                    @Override
                    public SecurityContext loadContext(HttpRequestResponseHolder requestResponseHolder) {
                        log.info("Perform loadContext.");
                        HttpServletRequest request = requestResponseHolder.getRequest();
                        SecurityContext context = readSecurityContextFromRequest(request);
                        String xtoken = CookieUtil.getCookieValue(request, "X-TOKEN");
                        if (null != xtoken && !"".equals(xtoken)) {
                            Authentication authentication = SimpleCacheUtil.getAuthentication(xtoken);
                            context.setAuthentication(authentication);
                        } else {
                            log.info("未登录或登录已失效");
                        }
                        request.setAttribute(SPRING_SECURITY_CONTEXT_KEY, context);
                        return context;
                    }

                    @Override
                    public void saveContext(SecurityContext context, HttpServletRequest request, HttpServletResponse response) {
                        if (null == context || context.getAuthentication() == null
                                || request.getAttribute(CONTEXT_SAVED) != null) {
                            return;
                        }
                        request.setAttribute(CONTEXT_SAVED, Boolean.TRUE);
                        log.info("Perform save token.");
                    }

                    @Override
                    public boolean containsContext(HttpServletRequest request) {
                        log.info("Perform containsContext.");
                        return request.getAttribute(SPRING_SECURITY_CONTEXT_KEY) != null;
                    }
                })
        ;
    }

    public CustomLoginFilter customAuthenticationProcessingFilter(AuthenticationManager authenticationManager) {
        return new CustomLoginFilter("/login", authenticationManager);
    }

    @Override
    protected AuthenticationManager authenticationManager() throws Exception {
        ProviderManager providerManager = new ProviderManager(Arrays.asList(
                authenticationProvider()
        ));
        providerManager.setEraseCredentialsAfterAuthentication(true);
        return providerManager;
    }

    @Bean
    public AuthenticationProvider authenticationProvider() {
        return new CustomAuthenticationProvider(userService);
    }


    /**
     * 在同一个Request中尝试获取SecurityContext
     */
    private SecurityContext readSecurityContextFromRequest(HttpServletRequest request) {
        Object contextFromSession = request.getAttribute(SPRING_SECURITY_CONTEXT_KEY);
        if (contextFromSession == null) {
            return generateNewContext();
        } else if (!(contextFromSession instanceof SecurityContext)) {
            return null;
        } else {
            return (SecurityContext) contextFromSession;
        }
    }

    protected SecurityContext generateNewContext() {
        return SecurityContextHolder.createEmptyContext();
    }
}
