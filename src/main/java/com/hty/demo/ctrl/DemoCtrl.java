package com.hty.demo.ctrl;

import com.alibaba.fastjson.JSON;
import com.hty.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@Controller
@RestController
@RequestMapping("/")
public class DemoCtrl {

    @Autowired
    private UserService userService;

    @RequestMapping("/demo")
    public @ResponseBody String demo(Principal principal) {

        return null == principal ? "{}" : JSON.toJSONString(principal);
    }
    @RequestMapping("/demo1")
    public @ResponseBody Integer demo1(Principal principal) {

        return userService.addFoundation(666);
    }
}
