package com.hty.demo.service;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.errorprone.annotations.Immutable;
import com.hty.demo.bean.po.CustomUserInfo;
import com.hty.demo.bean.po.UserInfo;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class UserService {

    private static final Map<String, String> simpleUserPasswordMap = new HashMap<>(1);
    private static final Map<String, Long> simpleUserRoleMap = new HashMap<>(1);

    static {
        simpleUserPasswordMap.put("admin", "123123");
        simpleUserRoleMap.put("admin", 1L);
    }

    public CustomUserInfo loadUserByUsername(String username) throws UsernameNotFoundException {
        if (simpleUserPasswordMap.containsKey(username)) {
            UserInfo userInfo = new UserInfo(username, username + "@foxless.com");
            //ImmutableSet.of(simpleUserRoleMap.get(username)),
            return new CustomUserInfo(simpleUserPasswordMap.get(username), ImmutableSet.of(simpleUserRoleMap.get(username)), userInfo);
        }
        return null;
    }

    // 方法级别的权限控制
    @PreAuthorize("hasAuthority('ROLE_1')")
    //@Secured("ROLE_1")
    public int addFoundation(int value) {
        return value;
    }
}
