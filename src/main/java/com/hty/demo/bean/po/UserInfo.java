package com.hty.demo.bean.po;

import java.util.Date;

public class UserInfo {
    private String username;
    private Date createTime;
    private String email;

    public UserInfo(String username, String email) {
        this.username = username;
        this.email = email;
        this.createTime = new Date();
    }

    public String getUsername() {
        return username;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public String getEmail() {
        return email;
    }
}
