package com.hty.demo.bean.po;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Set;
import java.util.stream.Collectors;

public class CustomUserInfo {

    private String password;
    private Set<GrantedAuthority> authorities;
    private UserInfo userInfo;

    public CustomUserInfo(String password, Set<Long> authorities, UserInfo userInfo) {
        this.password = password;
        this.userInfo = userInfo;
        if (null != authorities && !authorities.isEmpty()) {
            //this.authorities = ;
            this.authorities = authorities.stream().map(v-> new SimpleGrantedAuthority("ROLE_" + v)).collect(Collectors.toSet());
        }
    }

    public String getPassword() {
        return password;
    }

    public Set<GrantedAuthority> getAuthorities() {
        return authorities;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }
}
