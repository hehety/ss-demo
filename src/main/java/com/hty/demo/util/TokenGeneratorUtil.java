package com.hty.demo.util;

/**
 * 产生随机Token字符的工具
 */
public class TokenGeneratorUtil {

    private static char[] seeds = {
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't',
            'u', 'v', 'w', 'x', 'y', 'z',
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
            'U', 'V', 'W', 'X', 'Y', 'Z',
            '+', '-', '.',
    };

    private static char[] app_token_seeds = {
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
            'U', 'V', 'W', 'X', 'Y', 'Z', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0'
    };

    /**
     * 产生50-55位之间的随机字符串作为token
     */
    public static String generate() {
        int cut = (int) Math.floor(Math.random() * 5);
        int len = 200 - cut;
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < len; i++) {
            int index = (int) Math.floor(Math.random() * seeds.length);
            sb.append(seeds[index]);
        }
        return sb.toString();
    }

    public static String generateAppToken() {
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < 64; i++) {
            int index = (int) Math.floor(Math.random() * app_token_seeds.length);
            sb.append(seeds[index]);
        }

        return sb.toString().toUpperCase();
    }

	public static void main(String[] args) {
		System.out.println(generate());
		System.out.println(generateAppToken());
	}
}
