package com.hty.demo.util;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;

import java.util.HashMap;
import java.util.Map;

/**
 * a simple in-memory authentication cache util.
 */
public class SimpleCacheUtil {

    private static final Logger log = LoggerFactory.getLogger(SimpleCacheUtil.class);

    private static final Map<String, Authentication> cache = new HashMap<>();


    public static void cacheAuthentication(String sessionId, Authentication authentication) {
        cache.put(sessionId, authentication);
    }

    public static Authentication getAuthentication(String sessionId) {
        return cache.get(sessionId);
    }
}
