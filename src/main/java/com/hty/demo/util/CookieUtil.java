package com.hty.demo.util;


import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

public class CookieUtil {

    public static Cookie[] getCookies (HttpServletRequest req) {
        return req.getCookies();
    }

    public static String getCookieValue(HttpServletRequest req, String name) {
        Cookie[] cookies = getCookies(req);
        if (null != cookies) {
            for (Cookie c : cookies) {
                if (c.getName().equals(name)) {
                    return c.getValue();
                }
            }
        }
        return null;
    }

}
